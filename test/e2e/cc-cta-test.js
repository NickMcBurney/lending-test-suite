var conf = require('../../nightwatch.conf.js');
var data = require('../data/cc-data.json')

module.exports = {
    '@tags': ['cc', 'cta'],
    before : function (browser) {
        // set window to mobile size
        browser.resizeWindow(1360, 960)
    }, 
       
    'CC - Check CTA Product': function (browser) {
        browser
        // go to credit cards product page
        .url('https://www.oceanfinance.co.uk/credit-cards/')
        // close cookie message
        .waitForElementVisible('#cookie-button', 'Waiting cookie message to be visible - to close')
        .click('#cookie-button')
        // wait  - until cta button visible
        .waitForElementVisible('.btn.cta', 'Wait for cta to be visible then click')
        //.pause(1000)
        // click - the cta
        .click('.btn.cta')
        // wait - for the element to not be present
        // to confirm page changed
        .waitForElementNotPresent('.btn.cta', 10000, 'Wait for the page to change - start')
        .pause(2000)
        // wait - for body to load, 6s max wait
        .waitForElementVisible('body', 6000, 'Wait for the page to change - end')
        // check - page url contains expected form url
        .assert.urlContains('apply-for-a-credit-card', 'Redirected for form page')
        .end();
    },
    'CC - Check CTA Landing': function (browser) {
        browser
        // go to credit cards product page
        .url('https://www.oceanfinance.co.uk/landing/credit-cards/')
        // close cookie message
        //.waitForElementVisible('#cookie-button', 'Waiting cookie message to be visible - to close')
        //.click('#cookie-button')
        // wait  - until cta button visible
        .waitForElementVisible('.hero-section .btn', 'Wait for cta to be visible then click')
        //.pause(1000)
        // click - the cta
        .click('.hero-section .btn')
        // wait - for the element to not be present
        // to confirm page changed
        .waitForElementNotPresent('.hero-section .btn', 10000, 'Wait for the page to change - start')
        .pause(2000)
        // wait - for body to load, 6s max wait
        .waitForElementVisible('body', 6000, 'Wait for the page to change - end')
        // check - page url contains expected form url
        .assert.urlContains('apply-for-a-credit-card', 'Redirected for form page')
        .end();
    }
}