var conf = require('../../nightwatch.conf.js');
var data = require('../data/cc-data.json')

module.exports = {
    '@tags': ['cc', 'happy', 'forms'],
    before : function (browser) {
        // set window to mobile size
        browser.resizeWindow(360, 575)
    }, 
    'CC - Form Happy Path': function (browser) {
        browser
        // go to credit cards form page
        .url('https://www.oceanfinance.co.uk/credit-cards/apply-for-a-credit-card/')
        // wait - for body to load
        .waitForElementVisible('body')
        // close cookie message
        .waitForElementVisible('#cookie-button')
        .click('#cookie-button')
        // wait - for form to load
        .waitForElementVisible('form')
        .pause(750)

        // select title value
        .click('label[for='+ data.title + ']')
        .pause(250)

        // enter first name
        .setValue('#forename', data.first_name)
        .pause(250)

        // enter surname
        .setValue('#surname', data.last_name)
        .pause(250)

        // enter DOB
        .setValue('#day', data.dob.day)
        .setValue('#month', data.dob.month)
        .setValue('#year', data.dob.year)
        .click('body') // blur year input to trigger full date update
        // check dob value as expected
        .assert.value('#dateOfBirth', data.dob.date)
        .pause(250)
        // take screenshot of personal details section
        .saveScreenshot(conf.imgpathsimple(browser) + 'personal-details.png')

        // set lives in UK value
        .click('label[for=liveukgroupYes]')
        .assert.value('input[name=liveukgroupBtns]:checked', data.live_in_the_uk)
        .pause(250)

        // set address
        // set postcode
        .setValue('#postcode1', data.addresses[0].postcode)
        // click 'find address
        .click('#findAddress1')
        // wait for address results select box - max wait 6s
        .waitForElementVisible('#addressDropdown1', 6000)
        // click the address results select box
        .click('#addressDropdown1')
        // select the specified house number option
        .click('#addressDropdown1 option[data-housenumber=\'' + data.addresses[0].house_no + ', \' ]')
        // wait for selected address block to be shown
        .waitForElementVisible('#selected-address1')
        // wait for the years live question to be shown
        .waitForElementVisible('#liveyear1')
        // set number of years lived at address
        .setValue('#liveyear1', data.addresses[0].years_lived)
        // confirm the address
        .click('#confirmAddress1')
        // take screenshot of address block section
        .saveScreenshot(conf.imgpathsimple(browser) + 'address-block.png')
        .pause(250)

        // set residential status
        .click('label[for=residentialStatusBtnsHomeowner]')
        .assert.value('input[name=residentialStatusBtns]:checked', data.residential_status)
        .pause(250)

        // set email address
        .setValue('#email', data.email)
        .pause(250)

        // set mobile phone number
        .setValue('#mphonenumber', data.phone.mobile)
        .pause(250)

        // set employment status
        .click('label[for=Employed]')
        .assert.value('input[name=EmploymentStatus]:checked', data.employment.status)
        .pause(250)

        // set occupation
        .setValue('#Occupation', data.employment.occupation)
        .pause(250)

        // set annual income
        .setValue('#income', data.employment.annual_income)
        .pause(250)

        // set gdpr post to false
        .click('label[for=consentMarketingPostBtnNo]')
        .assert.value('input[name=consentMarketingPostBtn]:checked', 'No')
        .pause(250)

        // set gdpr phone to false
        .click('label[for=consentMarketingPhoneBtnNo]')
        .assert.value('input[name=consentMarketingPhoneBtn]:checked', 'No')
        .pause(250)

        // set gdpr text to false
        .click('label[for=consentMarketingSmsBtnNo]')
        .assert.value('input[name=consentMarketingSmsBtn]:checked', 'No')
        .pause(250)

        // set gdpr email to false
        .click('label[for=consentMarketingEmailBtnNo]')
        .assert.value('input[name=consentMarketingEmailBtn]:checked', 'No')
        .pause(250)

        // submit the form
        .click('#submitButton')
        // check loading screen
        .waitForElementVisible('#loadingOverlay')
        // doesnt work due to unpredicable timing 
        // e.g. redirect before full loop
        //.waitForElementVisible('#loadingOverlay .1', 20000)
        //.waitForElementVisible('#loadingOverlay .2', 20000)
        //.waitForElementVisible('#loadingOverlay .3', 20000)


        .end();
    }
}